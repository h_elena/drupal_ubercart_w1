# README #
This module is designed to receive payment orders for CMS Drupal module Commerce

=== Wallet One Payment Ubercart ===
Contributors: Wallet One
Version: 2.0.0
Tags: Wallet One, Drupal, buy, payment, payment for ubercart, wallet one integration, Ubercart
Requires at least: 4.30
Tested up to: 4.53
Stable tag: 4.53
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One module is a payment system for CMS DIAFAN. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on CMS Drupal, then you need a module payments for orders made. This will help you plug the payment system Wallet One. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

== Installation ==
1. Register on the site http://www.walletone.com
2. Download the module files on the site.
3. Instructions for configuring the plugin is in the file read.pdf.

== Screenshots ==
Screenshots are to be installed in the file read.pdf

== Changelog ==
= 1.0.1 =
* Fix - Fixed bug with return answer for payment system

= 2.0.0 =
* Added new universal classes

= 3.0.0 =
* Changed the structure of the plugin.
* Changed a handler of return at the payment system

== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates